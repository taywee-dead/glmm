/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "program.hxx"

namespace GLmm
{
    Program::Program()
    {
        debug = GL_TRUE;
        program = 0;
        linkStatus = GL_FALSE;
        exists = GL_FALSE;
    }

    Program::~Program()
    {
    }

    GLboolean Program::Create()
    {
        program = glCreateProgram();
        if (glIsProgram(program))
        {
            exists = GL_TRUE;
        } else
        {
            exists = GL_FALSE;
        }
        return (exists);
    }

    GLvoid Program::Destroy()
    {
        if (exists)
        {
            glDeleteProgram(program);
        }
        program = 0;
        linkStatus = GL_FALSE;
        exists = GL_FALSE;
    }

    std::string Program::GetLog()
    {
        return (log);
    }

    GLvoid Program::SetDebug(const GLboolean& debug)
    {
        this->debug = debug;
    }

    GLboolean Program::GetDebug()
    {
        return (debug);
    }

    GLboolean Program::Exists()
    {
        return (exists);
    }

    GLboolean Program::GetLinkStatus()
    {
        return (linkStatus);
    }

    GLint Program::GetAttribute(const GLchar* name)
    {
        return (glGetAttribLocation(program, name));
    }

    GLint Program::GetUniform(const GLchar* name)
    {
        return (glGetUniformLocation(program, name));
    }

    GLuint Program::GetUniformBlock(const GLchar* name)
    {
        return (glGetUniformBlockIndex(program, name));
    }

    GLvoid Program::AttachShader(Shader* shader)
    {
        // Do not allow attaching the same shader twice
        for (std::list<Shader*>::iterator it = shaders.begin(); it != shaders.end(); it++)
        {
            if ((*it) == shader)
            {
                return;
            }
        }
        shaders.push_back(shader);
    }

    GLvoid Program::DetachShader(Shader* shader)
    {
        for (std::list<Shader*>::iterator it = shaders.begin(); it != shaders.end(); it++)
        {
            if ((*it) == shader)
            {
                shaders.erase(it);
                return;
            }
        }
    }

    GLvoid Program::DetachAllShaders()
    {
        shaders.clear();
    }

    GLvoid Program::DestroyAttachedShaders()
    {
        for (std::list<Shader*>::iterator it = shaders.begin(); it != shaders.end(); it++)
        {
            (*it)->Destroy();
            delete (*it);
        }

        shaders.clear();
    }

    GLboolean Program::Link()
    {
        GLint linked;

        // Detaches all attached shaders before linking
        if (glIsProgram(program))
        {
            GLsizei shaderCount;
            GLuint shaderArray[16];
            glGetAttachedShaders(program, 16, &shaderCount, shaderArray);
            for (unsigned int i = 0; i < shaderCount; i++)
            {
                glDetachShader(program, shaderArray[i]);
            }
        }

        if (debug)
        {
            this->log = std::string();
        }

        // Attaches all shaders in list
        for (std::list<Shader*>::iterator it = shaders.begin(); it != shaders.end(); it++)
        {
            glAttachShader(program, (*it)->shader);
        }

        glLinkProgram(program);

        glGetProgramiv(program, GL_LINK_STATUS, &linked);

        if (linked)
        {
            linkStatus = GL_TRUE;
            glUseProgram(program);
        } else
        {
            linkStatus = GL_FALSE;
            if (debug)
            {
                GLint length;
                GLchar* log;
                glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
                log = new GLchar[length];
                glGetProgramInfoLog(program, length, &length, log);
                this->log = std::string(log);
                delete[] (log);
            }
        }
        return (linkStatus);
    }

    GLvoid Program::Use()
    {
        glUseProgram(program);
    }

    GLvoid Program::Unuse()
    {
        glUseProgram(0);
    }
}

