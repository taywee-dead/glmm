/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "shader.hxx"

namespace GLmm
{

    Shader::Shader()
    {
        debug = GL_TRUE;
        shader = 0;
        type = GL_VERTEX_SHADER;
        compileStatus = GL_FALSE;
        exists = GL_FALSE;
    }

    Shader::Shader(const std::string& source, const GLenum& type)
    {
        Shader();
        this->source = source;
        this->type = type;
    }

    Shader::~Shader()
    {
    }

    GLboolean Shader::Create()
    {
        shader = glCreateShader(type);
        if (glIsShader(shader))
        {
            exists = GL_TRUE;
        } else
        {
            exists = GL_FALSE;
        }
        return (exists);
    }

    GLvoid Shader::Destroy()
    {
        if (exists)
        {
            glDeleteShader(shader);
            exists = GL_FALSE;
        }
        shader = 0;
        compileStatus = GL_FALSE;
    }

    GLvoid Shader::SetSource(const std::string& source)
    {
        this->source = source;
    }

    std::string Shader::GetSource()
    {
        return (source);
    }

    std::string Shader::GetLog()
    {
        return (log);
    }

    GLvoid Shader::SetDebug(const GLboolean& debug)
    {
        this->debug = debug;
    }

    GLboolean Shader::GetDebug()
    {
        return (debug);
    }

    GLvoid Shader::SetType(const GLenum& type)
    {
        this->type = type;
    }

    GLenum Shader::GetType()
    {
        return (type);
    }

    GLboolean Shader::Exists()
    {
        return (exists);
    }

    GLboolean Shader::GetCompileStatus()
    {
        return (compileStatus);
    }

    GLboolean Shader::LoadFile(const std::string& fileName)
    {
        std::ifstream inFile;
        inFile.open(fileName.c_str(), std::fstream::in);
        if (!inFile.is_open())
        {
            return (GL_FALSE);
        }

        GLchar temp;
        source = std::string();

        while (GL_TRUE)
        {
            temp = inFile.get();
            if (inFile.eof())
            {
                break;
            }
            source.append(1, temp);
        }

        return (GL_TRUE);
    }

    GLboolean Shader::Compile()
    {
        GLint compiled;
        GLchar* csource = const_cast<GLchar*>(source.c_str());

        glShaderSource(shader, 1, const_cast<const GLchar**>(&csource), NULL);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (compiled)
        {
            compileStatus = GL_TRUE;
        } else
        {
            compileStatus = GL_FALSE;
            if (debug)
            {
                GLint length;
                GLchar* log;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
                log = new GLchar[length];
                glGetShaderInfoLog(shader, length, &length, log);
                this->log = std::string(log);
                delete[] (log);
            }
        }

        return (compileStatus);
    }
}

