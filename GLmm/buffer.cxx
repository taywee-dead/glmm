/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "buffer.hxx"

namespace GLmm
{
    Buffer::Buffer()
    {
    }

    Buffer::~Buffer()
    {
    }

    GLvoid Buffer::Create()
    {
        glGenBuffers(1, &buffer);
    }

    GLvoid Buffer::InitializeMemory(const GLenum& target, const GLsizeiptr& bufferSize, const GLvoid* data, const GLenum& hint)
    {
        glBufferData(target, bufferSize, data, hint);
    }

    GLvoid Buffer::SubMemory(const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data)
    {
        glBufferSubData(target, offset, size, data);
    }


    GLvoid Buffer::Destroy()
    {
        glDeleteBuffers(1, &buffer);
    }

    GLvoid Buffer::Bind(const GLenum& target) const
    {
        glBindBuffer(target, buffer);
    }

    GLuint Buffer::GetBuffer() const
    {
        return (buffer);
    }
}

