/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef VERTEXARRAY_HXX
#define VERTEXARRAY_HXX

#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "attributebuffer.hxx"
#include "elementbuffer.hxx"

namespace GLmm
{
    /**
     * \brief Class which wraps an OpenGL Vertex Array Object (VAO).
     *
     * Also contains a list of Buffers, which are managed externally.  
     * Attempts to simplify the process, while staying close to the original OpenGL functionality.
     * This class attempts to be as efficient as possible, and therefore does not do any safety checking.
     */
    class VertexArray
    {
        private:
            GLuint vertexArray;

            /**
             * \brief Anonymous struct that holds a single element buffer, which is a special use buffer
             */
            const ElementBuffer* elementBuffer;

        public:
            /**
             * \brief Does nothing.
             *
             * Call Create() to actually create the VAO.
             */
            VertexArray();

            /**
             * \brief Does nothing.
             *
             * Call Destroy() to actually destroy the VAO.
             */
            virtual ~VertexArray();

            /**
             * \brief Generates the VAO
             *
             * This must be called prior to using the object or attaching any buffers
             */
            GLvoid Create();

            /**
             * \brief Deletes the VAO
             *
             * This should be called before the destruction of the object, otherwise you will be
             * wasting valuable memory.  This does not delete any attached buffers.
             */
            GLvoid Destroy();

            /**
             * \brief Detach all attached buffers.
             */
            GLvoid DetachAllBuffers();

            /**
             * Bind the VAO for use.
             */
            GLvoid Bind() const;

            /**
             * \brief attaches an attribute buffer to the VertexArray, binding it automatically to its contained index
             *
             * If the buffer is already attached to the VAO, it will be removed and reattached, which primarily
             * will act to change its bound index.  Can probably be better achieved with an unordered map of the
             * style [GLint index => AttributeBuffer* buffer], but I'll worry about that another time.
             *
             * \param buffer the buffer to attach and bind
             * \param index the index to bind to (Likely from Program)
             * \param size the size of the datatype (number of members)
             * \param type the type of date (eg. GL_FLOAT)
             * \param normalized whether to map the input as normalized or to convert literally
             * \param integer whether the CLIENT SIDE attribute is an integer, if GL_TRUE, normalized is ignored.
             * \param stride distance between "chunks" of data
             * \param offset distance into stride the current attached attribute is
             */
            GLvoid Attach(const AttributeBuffer* buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLboolean integer, GLsizei stride, const GLvoid* offset);

            /**
             * \brief attaches an attribute buffer to the VertexArray, binding it automatically to its contained index
             *
             * If the buffer is already attached to the VAO, it will be removed and reattached, which primarily
             * will act to change its bound index.  Can probably be better achieved with an unordered map of the
             * style [GLint index => AttributeBuffer* buffer], but I'll worry about that another time.  This
             * version i for instancing.
             *
             * \param buffer the buffer to attach and bind
             * \param index the index to bind to (Likely from Program)
             * \param size the size of the datatype (number of members)
             * \param type the type of date (eg. GL_FLOAT)
             * \param normalized whether to map the input as normalized or to convert literally
             * \param integer whether the CLIENT SIDE attribute is an integer, if GL_TRUE, normalized is ignored.
             * \param stride distance between "chunks" of data
             * \param offset distance into stride the current attached attribute is
             * \param divisor number of instances to draw before moving on (for instanced rendering)
             */
            GLvoid Attach(const AttributeBuffer* buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLboolean integer, GLsizei stride, const GLvoid* offset, GLuint divisor);

            /**
             * \brief attaches an attribute buffer to the VertexArray, binding it automatically as an element array
             *
             * This will replace any currently bound ElementBuffer.
             *
             * \param buffer the buffer to attach and bind
             */
            GLvoid Attach(const ElementBuffer* buffer);

            /**
             * \brief Detaches an AttributeBuffer
             *
             * Detaches the specified buffer.
             *
             * \param index the index to remove
             */
            GLvoid Detach(GLuint index);

            /**
             * \brief Detaches an  ElementBuffer
             *
             * Changes the attached ElementBuffer to a nullptr
             */
            GLvoid DetachElementBuffer();

            /**
             * Unbind any bound VAO.
             */
            static GLvoid Unbind();

            /**
             * \brief Draw VAO to the screen using an Element Buffer.
             *
             * This requires an ElementBuffer to have been attached in order to draw the VAO to the screen.
             */
            GLvoid DrawElements() const;

            /**
             * \brief Draw VAO to the screen using an Element Buffer.
             *
             * This requires an ElementBuffer to have been attached in order to draw the VAO to the screen.
             * This makes use of instanced rendering.
             */
            GLvoid DrawElementsInstanced(GLsizei instances) const;

            /**
             * \brief Draw VAO to the screen using all bound arrays sequentially.
             *
             * This will not use any attached Element buffer.
             *
             * \param mode drawing mode, like GL_QUADS or GL_TRIANGLE_STRIP
             * \param count number of elements to call
             */
            GLvoid DrawArrays(const GLenum& mode, const GLsizei& count) const;

            /**
             * \brief Draw VAO to the screen using all bound arrays sequentially.
             *
             * This will not use any attached Element buffer.  This makes use of instanced rendering.
             *
             * \param mode drawing mode, like GL_QUADS or GL_TRIANGLE_STRIP
             * \param count number of elements to call
             */
            GLvoid DrawArraysInstanced(const GLenum& mode, const GLsizei& count, GLsizei instances) const;
    };
}
#endif
