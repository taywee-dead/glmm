/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "elementbuffer.hxx"

namespace GLmm
{
    GLvoid ElementBuffer::InitializeMemory(const GLsizeiptr& bufferSize, const GLvoid* data, const GLenum& hint)
    {
        Bind();
        Buffer::InitializeMemory(GL_ELEMENT_ARRAY_BUFFER, bufferSize, data, hint);
    }

    GLvoid ElementBuffer::SubMemory(const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data)
    {
        Bind();
        Buffer::SubMemory(GL_ELEMENT_ARRAY_BUFFER, offset, size, data);
    }

    GLvoid ElementBuffer::Bind() const
    {
        Buffer::Bind(GL_ELEMENT_ARRAY_BUFFER);
    }

    GLvoid ElementBuffer::SetType(const GLenum& type)
    {
        this->type = type;
    }

    GLvoid ElementBuffer::SetMode(const GLenum& mode)
    {
        this->mode = mode;
    }

    GLvoid ElementBuffer::SetCount(const GLsizei& count)
    {
        this->count = count;
    }


    GLenum ElementBuffer::GetType() const
    {
        return (type);
    }

    GLenum ElementBuffer::GetMode() const
    {
        return (mode);
    }

    GLsizei ElementBuffer::GetCount() const
    {
        return (count);
    }
}

