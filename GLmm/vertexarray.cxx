/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "vertexarray.hxx"

namespace GLmm
{
    VertexArray::VertexArray()
    {
        elementBuffer = nullptr;
    }

    VertexArray::~VertexArray()
    {
    }

    GLvoid VertexArray::Create()
    {
        glGenVertexArrays(1, &vertexArray);
    }

    GLvoid VertexArray::Destroy()
    {
        Unbind();
        glDeleteVertexArrays(1, &vertexArray);
    }

    GLvoid VertexArray::Bind() const
    {
        glBindVertexArray(vertexArray);
    }

    GLvoid VertexArray::Unbind()
    {
        glBindVertexArray(0);
    }

    GLvoid VertexArray::Attach(const AttributeBuffer* buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLboolean integer, GLsizei stride, const GLvoid* offset)
    {
        Bind();
        buffer->Bind();
        if (integer)
        {
            glVertexAttribIPointer(index, size, type, stride, offset);
        } else
        {
            glVertexAttribPointer(index, size, type, normalized, stride, offset);
        }
        glEnableVertexAttribArray(index);
    }

    GLvoid VertexArray::Attach(const AttributeBuffer* buffer, GLuint index, GLint size, GLenum type, GLboolean normalized, GLboolean integer, GLsizei stride, const GLvoid* offset, GLuint divisor)
    {
        Bind();
        buffer->Bind();
        if (integer)
        {
            glVertexAttribIPointer(index, size, type, stride, offset);
        } else
        {
            glVertexAttribPointer(index, size, type, normalized, stride, offset);
        }
        glVertexAttribDivisor(index, divisor);
        glEnableVertexAttribArray(index);
    }

    GLvoid VertexArray::Attach(const ElementBuffer* buffer)
    {
        Bind();
        this->elementBuffer = buffer;
        buffer->Bind();
    }

    GLvoid VertexArray::Detach(GLuint index)
    {
        Bind();
        glDisableVertexAttribArray(index);
    }

    GLvoid VertexArray::DetachElementBuffer()
    {
        elementBuffer = nullptr;
    }

    GLvoid VertexArray::DrawElements() const
    {
        elementBuffer->Bind();
        glDrawElements(elementBuffer->GetMode(), elementBuffer->GetCount(), elementBuffer->GetType(), NULL);
    }

    GLvoid VertexArray::DrawElementsInstanced(GLsizei instances) const
    {
        elementBuffer->Bind();
        glDrawElementsInstanced(elementBuffer->GetMode(), elementBuffer->GetCount(), elementBuffer->GetType(), NULL, instances);
    }

    GLvoid VertexArray::DrawArrays(const GLenum& mode, const GLsizei& count) const
    {
        glDrawArrays(mode, 0, count);
    }

    GLvoid VertexArray::DrawArraysInstanced(const GLenum& mode, const GLsizei& count, GLsizei instances) const
    {
        glDrawArraysInstanced(mode, 0, count, instances);
    }
}

