/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "attributebuffer.hxx"

namespace GLmm
{
    GLvoid AttributeBuffer::InitializeMemory(const GLsizeiptr& bufferSize, const GLvoid* data, const GLenum& hint)
    {
        Bind();
        Buffer::InitializeMemory(GL_ARRAY_BUFFER, bufferSize, data, hint);
    }

    GLvoid AttributeBuffer::SubMemory(const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data)
    {
        Bind();
        Buffer::SubMemory(GL_ARRAY_BUFFER, offset, size, data);
    }

    GLvoid AttributeBuffer::Bind() const
    {
        Buffer::Bind(GL_ARRAY_BUFFER);
    }
}

