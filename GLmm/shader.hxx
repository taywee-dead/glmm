/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef SHADER_HXX
#define SHADER_HXX

#include <string>
#include <fstream>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

namespace GLmm
{
    /**
     * \brief Class which wraps a GLSL shader
     *
     * Attempts to simplify the process, while staying close to the original OpenGL functionality.
     */
    class Shader
    {
        friend class Program;

        private:
            GLuint shader;
            GLboolean exists;
            GLenum type;
            GLboolean compileStatus;
            // debug turns on and off logging;
            GLboolean debug;
            std::string source;
            std::string log;

        public:

            /**
             * Useless, except for creating a few defaults, does not create the shader.
             * Use Create() in order to actually create the shader.
             */
            Shader();

            /**
             * Sets the source and shader type on construction.
             * Use Create() in order to actually create the shader.
             */
            Shader(const std::string& source, const GLenum& type);

            /**
             * Useless, does literally nothing.  Does not destroy the shader.
             * Use Destroy() to actually destroy the shader.
             */
            virtual ~Shader();

            /**
             * Creates the shader and sets some values.
             * By default, the shader is a vertex shader, call SetType() first to change this.
             *
             * \returns the success of the creation
             */
            GLboolean Create();

            /**
             * Destroys the shader and sets some values.
             */
            GLvoid Destroy();

            /**
             * Sets the source code.
             *
             * \param source the string which contains the entire source of the shader
             */
            GLvoid SetSource(const std::string& source);

            /**
             * \returns the entire source code as a std::string
             */
            std::string GetSource();

            /**
             * Get the compile log.  Only exists if the debug status is set te GL_TRUE
             *
             * \returns the compile log
             */
            std::string GetLog();

            /**
             * \brief Sets the debug status of the shader.
             *
             * This will determine whether a compile log is created and stored.  This only takes
             * effect at compile time.
             *
             * \param debug The boolean to be set
             */
            GLvoid SetDebug(const GLboolean& debug);

            /**
             * \returns The debug status of the shader
             */
            GLboolean GetDebug();

            /**
             * Sets the shader type.
             *
             * \param type set to either GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, or GL_GEOMETRY_SHADER
             */
            GLvoid SetType(const GLenum& type);

            /**
             * \param the shader type
             */
            GLenum GetType();

            /**
             * Tells whether the shader has been created.
             * Does not determine if the shader is compiled.
             *
             * \returns the existance status of the shader
             */
            GLboolean Exists();

            /**
             * \returns the compile status of the program
             */
            GLboolean GetCompileStatus();

            /**
             * Load an text file in as GLSL source code.
             *
             * \param fileName relative or absolute path to the source file
             * \returns load status of source file
             */
            GLboolean LoadFile(const std::string& fileName);

            /**
             * \brief Attempt to compile the shader.
             *
             * If debug is set, the compile log will be filled if necessary.
             *
             * \returns the compile status
             */
            GLboolean Compile();
    };
}
#endif
