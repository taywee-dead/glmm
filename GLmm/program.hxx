#ifndef PROGRAM_HXX
#define PROGRAM_HXX

#include <list>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "shader.hxx"

namespace GLmm
{
    /**
     * \brief Class which wraps a GLSL shader program
     *
     * Contains a list of shaders, and has every shader fully reattached only at link-time, to prevent
     * conflicts and shader change.
     * Attempts to simplify the process, while staying close to the original OpenGL functionality.
     */
    class Program
    {
        private:
            GLuint program;
            GLboolean exists;
            GLboolean linkStatus;

            // debug turns on and off logging;
            GLboolean debug;
            std::list<Shader*> shaders;
            std::string log;

        public:

            /**
             * Useless, except for creating a few defaults, does not create the program.
             * Use Create() in order to actually create the program.
             */
            Program();

            /**
             * Useless, does literally nothing.  Does not destroy the program.
             * Use Destroy() to actually destroy the program.
             */
            virtual ~Program();

            /**
             * Creates the program and sets some values.
             *
             * \returns the success of the creation
             */
            GLboolean Create();

            /**
             * Destroys the program and sets some values.
             * Does not destroy any attached shaders.
             * If shaders were attached that are not garbage-collected, call
             * DestroyAttachedShaders() first.
             */
            GLvoid Destroy();

            /**
             * Get the linker log.  Only exists if the debug status is set te GL_TRUE
             *
             * \returns the linker log
             */
            std::string GetLog();

            /**
             * \brief Sets the debug status of the program.
             *
             * This will determine whether a linker log is created and stored.  This only takes
             * effect at linking time.
             *
             * \param debug The boolean to be set
             */
            GLvoid SetDebug(const GLboolean& debug);

            /**
             * \returns The debug status of the program
             */
            GLboolean GetDebug();

            /**
             * Tells whether the program has been created.
             * Does not determine if the program is linked.
             *
             * \returns the existance status of the program
             */
            GLboolean Exists();

            /**
             * \returns the link status of the program
             */
            GLboolean GetLinkStatus();

            /**
             * \param name the name of the attribute
             * \returns the index of the attribute
             */
            GLint GetAttribute(const GLchar* name);

            /**
             * \param name the name of the uniform
             * \returns the index of the uniform
             */
            GLint GetUniform(const GLchar* name);

            /**
             * \param name the name of the uniform block
             * \returns the index of the uniform block
             */
            GLuint GetUniformBlock(const GLchar* name);

            /**
             * \brief Attach a shader to the program for linking.
             *
             * The shader does not need to be compiled when attached, but must be compiled
             * before linking.  Linking will not automatically attempt to compile the shaders.
             * A shader will not be attached twice.
             *
             * \param shader the shader to attempt to attach
             */
            GLvoid AttachShader(Shader* shader);

            /**
             * \brief Detach a shader from the program.
             * 
             * Only removes the reference, will not actually delete anything.
             *
             * \param shader the shader to attempt to detach
             */
            GLvoid DetachShader(Shader* shader);

            /**
             * \brief Detach all shaders from the program.
             * 
             * Only removes the references, will not actually delete anything.
             */
            GLvoid DetachAllShaders();

            /**
             * \brief Deletes all attached shaders.
             *
             * This will actually destroy all of the attached shaders, as well as deleting the objects
             * and the references.  Do not use this if you have garbage-collected shaders attached, or
             * you will crash your program with a segfault.
             */
            GLvoid DestroyAttachedShaders();

            /**
             * \brief Attempt to link the program.
             *
             * This will remove all attached shader objects from the program, attach the shaders in the
             * list, and attempt to link the program.  If debug is set, the linker log will be
             * filled when necessary.
             *
             * \returns the link status
             */
            GLboolean Link();

            /**
             * Set the program as the active program.
             */
            GLvoid Use();

            /**
             * Set the program as the default behavior (fixed functionalty).
             */
            static GLvoid Unuse();
    };
}
#endif
