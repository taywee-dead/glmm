/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef ATTRIBUTEBUFFER_HXX
#define ATTRIBUTEBUFFER_HXX

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#include "buffer.hxx"

namespace GLmm
{
    /**
     * \brief Class which wraps an OpenGL attribute buffer
     */
    class AttributeBuffer : public Buffer
    {
        public:
            /**
             * \brief Creates memory and fills it with data
             *
             * if data is NULL, the memory is created but not initialized
             *
             * \param bufferSize the size of the new buffer
             * \param data pointer to the data to initialize the memory, or NULL
             * \param hint usage hint, to determine efficiency in certain areas for memory
             */
            GLvoid InitializeMemory(const GLsizeiptr& bufferSize, const GLvoid* data, const GLenum& hint);

            /**
             * \brief substitutes memory with data
             *
             * \param offset the number of bytes into the memory to start filling
             * \param size the number of bytes to fill
             * \param data pointer to the data to fill the memory
             */
            GLvoid SubMemory(const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data);

            /**
             * Bind the buffer for use.
             */
            GLvoid Bind() const;
    };
}
#endif
