/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BUFFER_HXX
#define BUFFER_HXX

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

namespace GLmm
{
    /**
     * \brief Class which wraps an OpenGL buffer
     */
    class Buffer
    {
        protected:
            GLuint buffer;

        public:
            /**
             * \brief Does nothing.
             *
             * Call Create() to actually create the buffer.
             */
            Buffer();

            /**
             * \brief Does nothing.
             *
             * Call Destroy() to actually destroy the buffer.
             */
            virtual ~Buffer();

            /**
             * \brief Generates the Buffer
             *
             * This must be called prior to using the buffer or defining anything
             */
            GLvoid Create();

            /**
             * \brief Creates memory and fills it with data
             *
             * If data is NULL, the memory is created but not initialized.
             * This does not bind the object first.
             *
             * \param target the generic target for binding the data
             * \param bufferSize the size of the new buffer
             * \param data pointer to the data to initialize the memory, or NULL
             * \param hint usage hint, to determine efficiency in certain areas for memory
             */
            GLvoid InitializeMemory(const GLenum& target, const GLsizeiptr& bufferSize, const GLvoid* data, const GLenum& hint);

            /**
             * \brief substitutes memory with data
             *
             * This does not bind the object first.
             *
             * \param target the generic target for binding the data
             * \param offset the number of bytes into the memory to start filling
             * \param size the number of bytes to fill
             * \param data pointer to the data to fill the memory
             */
            GLvoid SubMemory(const GLenum& target, const GLintptr& offset, const GLsizeiptr& size, const GLvoid* data);

            /**
             * \brief Deletes the Buffer
             *
             * This should be called before the destruction of the object, otherwise you will be
             * wasting valuable memory
             */
            GLvoid Destroy();

            /**
             * Bind the buffer for use.
             *
             * \param target Specifies the target to which the buffer object is bound.  The symbolic constant must be GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, GL_PIXEL_PACK_BUFFER, or GL_PIXEL_UNPACK_BUFFER.
             */
            GLvoid Bind(const GLenum& target) const;

            /**
             * \brief Gets the buffer
             *
             * \retuns the integer representing the buffer object
             */
            GLuint GetBuffer() const;
    };
}
#endif
