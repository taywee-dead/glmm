Objects = attributebuffer.o buffer.o elementbuffer.o program.o shader.o vertexarray.o
ObjectsD = attributebuffer-d.o buffer-d.o elementbuffer-d.o program-d.o shader-d.o vertexarray-d.o

allObjects = $(Objects) $(ObjectsD)

Libraries = libGLmm.so libGLmm-d.so

CC = clang
CXX = clang++

CompileOptsAll = -c -fpic -Wall -std=c++11 -I.
CompileOpts = $(CompileOptsAll) -s -O2
CompileOptsD = $(CompileOptsAll) -g -O0

LinkerOptsAll = -Wall -fpic -std=c++11 -lGL -lGLEW
LinkerOpts = $(LinkerOptsAll) -O2 -s
LinkerOptsD = $(LinkerOptsAll) -O0 -g

.PHONY : all clean debug docs install install-docs release shared static uninstall uninstall-docs

all : $(Libraries)

install :
	-mv -v -t /usr/lib $(Libraries)
	cp -v --parents -t /usr/include `find . -iname "*.hxx"` 

install-docs : uninstall-docs
	-mkdir -p /usr/share/doc/GLmm
	mv -v ./latex ./html -t /usr/share/doc/GLmm
	mv -v ./man/man3/*.gz -t /usr/share/man/man3

uninstall :
	-rm -r /usr/include/GLmm
	-rm /usr/include/GLmm.hxx /usr/lib/libGLmm*.so

uninstall-docs :
	-rm -rv /usr/share/doc/GLmm
	-rm -rv /usr/share/man/man3/GLmm_*

clean :
	-rm -v $(allObjects) $(Libraries) 

docs : 
	doxygen
	rm -r ./man/man3/_*_.3
	gzip -f ./man/man3/*.3

libGLmm.so : $(Objects)
	$(CXX) -shared -o ./libGLmm.so $(Objects) $(LinkerOpts)
	
libGLmm-d.so : $(ObjectsD)
	$(CXX) -shared -o ./libGLmm-d.so $(ObjectsD) $(LinkerOptsD)

attributebuffer.o : GLmm/attributebuffer.cxx GLmm/attributebuffer.hxx GLmm/buffer.hxx
	$(CXX) $(CompileOpts) -o ./attributebuffer.o ./GLmm/attributebuffer.cxx
buffer.o : GLmm/buffer.cxx GLmm/buffer.hxx
	$(CXX) $(CompileOpts) -o ./buffer.o ./GLmm/buffer.cxx
elementbuffer.o : GLmm/elementbuffer.cxx GLmm/elementbuffer.hxx GLmm/buffer.hxx
	$(CXX) $(CompileOpts) -o ./elementbuffer.o ./GLmm/elementbuffer.cxx
program.o : GLmm/program.hxx GLmm/program.cxx GLmm/shader.hxx
	$(CXX) $(CompileOpts) -o ./program.o ./GLmm/program.cxx
shader.o : GLmm/shader.hxx GLmm/shader.cxx
	$(CXX) $(CompileOpts) -o ./shader.o ./GLmm/shader.cxx
vertexarray.o : GLmm/vertexarray.cxx GLmm/vertexarray.hxx GLmm/attributebuffer.hxx
	$(CXX) $(CompileOpts) -o ./vertexarray.o ./GLmm/vertexarray.cxx

attributebuffer-d.o : GLmm/attributebuffer.cxx GLmm/attributebuffer.hxx GLmm/buffer.hxx
	$(CXX) $(CompileOptsD) -o ./attributebuffer-d.o ./GLmm/attributebuffer.cxx
buffer-d.o : GLmm/buffer.cxx GLmm/buffer.hxx
	$(CXX) $(CompileOptsD) -o ./buffer-d.o ./GLmm/buffer.cxx
elementbuffer-d.o : GLmm/elementbuffer.cxx GLmm/elementbuffer.hxx GLmm/buffer.hxx
	$(CXX) $(CompileOptsD) -o ./elementbuffer-d.o ./GLmm/elementbuffer.cxx
program-d.o : GLmm/program.hxx GLmm/program.cxx GLmm/shader.hxx
	$(CXX) $(CompileOptsD) -o ./program-d.o ./GLmm/program.cxx
shader-d.o : GLmm/shader.hxx GLmm/shader.cxx
	$(CXX) $(CompileOptsD) -o ./shader-d.o ./GLmm/shader.cxx
vertexarray-d.o : GLmm/vertexarray.cxx GLmm/vertexarray.hxx GLmm/attributebuffer.hxx
	$(CXX) $(CompileOptsD) -o ./vertexarray-d.o ./GLmm/vertexarray.cxx

